(defproject scorecard-service "0.1.0-SNAPSHOT"
  :description "scorecard service"
  :source-paths ["src/clj"]
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-2311"]
                 [org.clojure/core.async "0.1.338.0-5c5012-alpha"]
                 [org.clojure/clojurescript  "0.0-2311"]
                 [compojure "1.1.8"]
                 [ring/ring-json "0.3.1"]
                 [org.clojure/data.json "0.2.5"]
                 [enlive "1.1.5"]
                 [clj-http "1.0.0"]
                 [com.novemberain/validateur "2.2.0"]
                 [metis "0.3.3"]
                 [org.clojure/java.jdbc "0.3.5"]
                 [org.xerial/sqlite-jdbc "3.7.2"]
                 [korma "0.4.0"]
                 [liberator "0.12.0"]
                 [reagent  "0.4.2"]
                 [javax.servlet/servlet-api  "2.5"] 
                 [cljs-ajax  "0.2.6"]
                 [clj-http "1.0.0"] 
                 [secretary "1.2.0"]
                 [org.clojure/tools.logging "0.3.0"]
                 [http-kit "2.1.19"]
                 [prone  "0.6.0"]]
  :jvm-opts ["-Xmx1G"]
  :plugins [[lein-ring "0.8.11"]
            [lein-cljsbuild  "1.0.3"] ]
  :cljsbuild  {:builds  [{:source-paths  ["src/cljs"]
                          :compiler  {:output-to  "resources/public/js/app.js"
                                      :optimizations :whitespace
                                      :preamble  ["reagent/react.js"]
                                      :pretty-print true}}]}
  :ring {
         :nrepl {:start? true}
         :init    scorecard-service.system/init
         :handler scorecard-service.api/handler
         :destroy scorecard-service.system/destroy}
  :aot :all
  :profiles {
   :dev {:repl-options {:init-ns scorecard-service.views}
         :dependencies [ [ring-mock "0.1.5"]]}
   :production  {:ring  {:open-browser? false, :stacktraces? false, :auto-reload? false}}})

