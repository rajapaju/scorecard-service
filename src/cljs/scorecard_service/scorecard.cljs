(ns scorecard-service.scorecard)

(defn scorecard [holes player]
  {:scores (map #( #{% ((key %) "par")}) (zipmap holes players ))
   :holes holes
   :total (zipmap players (repeat 0))})
