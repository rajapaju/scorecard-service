(ns scorecard-service.views
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require [clojure.string :as string]
            [reagent.core :as r]
            [ajax.core :as ajax]
            [secretary.core :as secretary :include-macros true :refer  [defroute]]
            [cljs.core.async :as async :refer [>! >!! <! <!!  put! chan alts! timeout]]
            [goog.events :as events]
            [goog.dom.classes :as classes])
  (:import [goog.events EventType]))

(enable-console-print!)

(def players-resource "players/")
(def games-resource "games/")
(def courses-resource "courses/")

(def courses (r/atom (list)))
(def game (r/atom {:playersAndScores []}))
(def gg (r/atom {}))
(def games (r/atom (list)))
(def players (r/atom (list)))
(def messages (r/atom (list)))
(def messages-channel (chan))

(def list-item-class "btn-group-vertical btn-group-lg btn-block")

(defn- response-handler
  ([resource-atom]
   (fn [response] (swap! resource-atom (fn [_] response))))
  ([resource-atom f]
   (fn [response] (swap! resource-atom (fn [_]  (f response))))))

(defn get-resource
  ([resource resource-atom]
   (ajax/GET resource {:format :json :handler (response-handler resource-atom)}))
  ([resource resource-atom f]
   (ajax/GET resource {:format :json :handler (response-handler resource-atom f)})))

(defn panel [title on-click & body]
  [:div {:class "row"}
   [:div {:class "col-md-12"}
    [:div {:class "panel panel-default"}
     [:div {:class "panel-heading" :on-click on-click}
      [:h3 {:class "panel-title"} title]]
     [:div {:class "panel-body"} body]]]])

(defn list-item [f title]
  [:button {:on-click f :class "btn btn-default"} title])

(defn menu-view  []
  [panel "Main Menu" #(menu-page)
   [:div {:class "text-center"}
    [:div {:class list-item-class}
     [list-item select-course-page "New Game"]
     [list-item games-list-page "Games"]
     [list-item players-list-page "Players"]
     [list-item courses-list-page "Courses"]
     [list-item statistics-page "Statistics"]
    [:div {:class list-item-class}
     [:button {:type "button" :class "btn btn-primary"} "Logout"]]]]])


(defn players-list-view  [list-atom]
  (if-let [players (not-empty @list-atom)]
    (fn [] [panel "Players" #(menu-page)
            [:div {:class list-item-class}
             (for [p players]
               [:a {:class "btn btn-default" :on-click #(player-page p)} (p "loginname")])]])
    [panel "Players" #(menu-page)
     [:div "Loading players..."]]))

(defn edit-player-view  [player]
  (fn []
    [panel (str "Player" @messages) #(players-list-page)
     [:form {:role "form"}
      (for [k (filter #(not= % "id") (keys @player))]
        [:div {:class "form-group"}
         [:label {} (str k) ":"]
         [:input {:type "text"
                  :class "form-control"
                  :value (str (@player k))
                  :onChange (fn [e] (swap! player assoc k (-> e .-target .-value)))}]])
      [:div {:class "row"}
       [:div {:class "col-xs-12"}
        [:button {:type "button" :class "btn btn-success btn-lg btn-block"
                  :on-click #(ajax/PUT
                               (str players-resource (@player "id"))
                               {:format :json
                                :handler (fn [resp] 
                                           (go 
                                             (>! messages-channel "juupa")
                                             (<! (timeout 2000))
                                             (.log js/console "hujii")
                                             (swap! messages rest)))
                                :params @player})} "Save"]] ]]]))

(defn courses-list-view  [list-atom]
  (fn [] [panel "Courses" #(menu-page)
          (if-let [courses (not-empty @list-atom)]
            [:div {:class list-item-class}
             (for [c courses]
               [:a {:class "btn btn-default"
                    :on-click #( render-page
                                 [course-page c])} (c "name")])]
            [:div "Loading courses..."])]))

(defn select-course-view [courses-atom]
  (fn [] [panel "Select Course" #(menu-page)
          (if-let [courses (not-empty @courses-atom) ]
            [:div {:class list-item-class}
             (for [course courses]
               [:a {:class "btn btn-default" :on-click #(select-players-page course) } (course "name")])]
            [:div "Loading courses..."])]))

(defn select-course-page []
  (get-resource courses-resource courses)
  (render-page [select-course-view courses]))

(defn hole-view [hole]
  [:tr
   [:td {:class "col-xs-2"} [:strong (hole "order")]]
   [:td {:class "col-xs-2"} [:strong (str "par " (hole "par"))]]
   [:td {:class "col-xs-6"} (hole "description")]
   [:td {:class "col-xs-2"} [:strong (str  (hole "distance") " m")]]])

(defn count-course-totals [course]
  (reduce (fn [v hole] {:course-distance (+ (:course-distance v) (hole "distance"))
              :course-par (+ (v :course-par) (hole "par")) }) {:course-distance -1 :course-par 0} (course "holes")))

(defn course-view [course]
  (fn []
    [panel "Course Information" #(courses-list-page)
     [:div
      [:h3 "General Info"]
      [:table {:class "table table-condensed"}
       [:tbody
        [:tr
         [:td {:class "col-xs-2"} [:strong "Course Name"]]
         [:td (@course "description")]]
        [:tr
         [:td [:strong "Description"]]
         [:td (@course "description")]]
        [:tr
         [:td [:strong "Distance"]]
         [:td (:course-distance (count-course-totals @course))]]
        [:tr
         [:td [:strong "Par"]]
         [:td (:course-par (count-course-totals @course))]]]]
      [:hr]
      [:h3 (str "Holes (" (count (@course "holes")) " pcs)")]
      [:table {:class "table table-condensed"}
       [:tbody
        (for [hole (@course "holes")]
          [hole-view hole])]]
      [:div {:class "col-md-12"}
       [:button {:type "button" :class "btn btn-primary btn-sm btn-block" :disabled "disabled"} "Save"]]]]))

(defn login-page  []
  (render-page
    [panel "Login" #(login-page)
     [:div {:class "form-signin" :role "form"}
      [:input {:type "text" :class "form-control" :placeholder "Login Name" :required "" :value (get-value :email) :autofocus="" :onChange #(set-value! :loginname (-> % .-target .-value))}]
      [:input {:type "password" :disabled "disabled"  :class "form-control" :placeholder "Password" :required "" :value (get-value :password) :onChange #(set-value! :password (-> % .-target .-value))}]
      [:button {:class "btn btn-lg btn-primary btn-block" :type "submit" :on-click #(menu-page)} "Sign in"]]]))

(defn update-selected-players [player selected-players]
  (cond (nil? (some #{player} selected-players))
        (conj selected-players player)
        :else  (remove #{player} selected-players)))

(defn swap-result! [game-atom hole player score f]
  (.log js/console (str "scores-map " ( @game-atom "scores-map")))
  (swap! game-atom assoc-in ["scores-map" #{(player "id") (hole "id")}] (f score)))


(defn hole-scores [game-atom]
  (fn []
    (.log js/console (str "scores-map " ( @game-atom "scores-map")))
    [:table {:class "table table-striped table-condensed"}
     [:tbody
      (let [players (@game-atom "players")
            course (@game-atom "course")
            holes (course "holes")
            hole (@game-atom "current-hole")
            par (hole "par") 
            scores (@game-atom "scores")
            scores-m (@game-atom "scores-map")]
        (.log js/console "miksi ei tule mitään " (str scores-m))
        (for [player players]
          (let [k #{(hole "id") (player "id")} score (get scores-m k par)] 
            (.log js/console (str player "=" score k))
            [:tr
           [:td {:class "col-xs-7 text-align-middle"} (player "loginname")]
           [:td {:class "col-xs-1 text-align-middle"} (str "(" par ")")]
           [:td {:class "col-xs-1"}
            [:button {:type "button" :class (str "btn btn-default" (cond (< 1 score) "" :else " disabled")) :on-click #(swap-result! game-atom hole player score dec)}
             [:span {:class "fa fa-minus"}]]]

           [:td {:class "col-xs-1 text-align-middle"} (str score " (" (- score (hole "par")) ")")]
           [:td {:class "col-xs-1"}
            [:button {:type "button" :class "btn btn-default" :on-click #(swap-result! game-atom hole player score inc)}
             [:span {:class "fa fa-plus"}]]]
           [:td {:class "col-xs-1 text-align-middle"} (reduce (fn [acc hole] (+ acc (get (@game-atom "scores-map") #{(hole "id") (player "id")} 0))) 0 holes)]
           [:td]])))]]))

(defn get-current-hole [holes scores]
  (cond
    (empty scores) (first holes)
    :else (first (filter #(complement (some (fn [score] (= (score "holes_id") (% "id"))) scores)) holes)))
  )

(defn game-view [game-atom]
  (fn []
    (if-let [game @game-atom]
      (let [course (game "course") 
            holes (course "holes")
            scores (game "scores")
            current-hole (game "current-hole") 
            par (current-hole "par")]
        [panel (str (course "name" ) ", Hole " (current-hole "number") "/" (count holes) ", Par " par) #(games-list-page)
         (cond (not= current-hole (first holes))
               [:button {:class "btn btn-lg btn-primary btn-block"
                         :on-click #(swap! game-atom assoc "current-hole" (last (take-while (complement #{current-hole}) holes)))} "Prev"])
         (cond (= current-hole (last holes))
               [:button {:class "btn btn-lg btn-success btn-block"
                         :on-click #(overview-page game-atom)} "Finnish"])
         (cond (not= current-hole (last holes))
               [:button {:class "btn btn-lg btn-primary btn-block"
                         :on-click #(swap! game-atom assoc "current-hole" (second (drop-while (complement #{current-hole}) holes)))} "Next"])
         [hole-scores game-atom]])
      [panel "Game" #(menu-page)
       [:div "Loading Game..."]])))

(defn games-list-view [games-atom]
  (.log js/console (str "list games " @games-atom))
  (if-let [list-of-games (not-empty @games-atom)]
    [panel "Games" #(menu-page)
     [:div {:class "text-center"}
      [:div {:class list-item-class}
       (for [game list-of-games]
         (list-item #(game-page game) (str (game "name")  " " (game "created")) ))]]]
    [panel "Games" #(menu-page)
     [:div "Loading games..."]]))

(defn start-game  [course selected-players]
  (ajax/POST games-resource {:format :json
              :handler (fn [game] (game-page game))
              :params {:courseid (course "id") :playerids (map #(% "id") selected-players)}}))

(defn overview-table-view [game-atom]
  (let [players (game-atom "players")
        course (game-atom "course")
        holes (course "holes")
        scores-map (game-atom "scores-map") ]
    [:table {:class "table table-responsive table-striped text-center"}
     [:thead
      [:tr
       [:th {:class "col-xs-2"}]
       (for [player players]
         [:th {:class "col-xs-1"} (player "loginname")]) ]]
     [:tbody
      (for [hole holes]
        [:tr
         [:td {:class "result"} (str "Hole " (hole "number"))]
         (for [player players]
           [:td {:class "result"} (str (get scores-map #{(hole "id") (player "id")} ))])])]
     (let [course-total (reduce #(+ %1 (%2 "par")) 0 holes)]
       [:tfoot
        [:tr
         [:td {:class "result"} "Result"]
         (for [player players]
           [:td {:class "result"}
            (let [player-total (- (reduce (fn [acc hole] (+ acc (get scores-map #{(hole "id") (player "id")} 0))) 0 holes) course-total)]
              (str (cond (neg? player-total) "" :else "+") player-total))])]
        [:tr
         [:td {:class "result"} "Tolal"]
         (for [player players]
           [:td {:class "result"}
            (let [player-total (reduce (fn [acc hole] (+ acc (get scores-map #{(hole "id") (player "id")} 0))) 0 holes)]
              player-total)])]
        ])]))
;;
;; Select players
;;
(defn players-list-item [p selected-players]
   (fn [] [:a {:class (str "btn " (cond (nil? (some #{p} @selected-players)) "btn-default" :else "btn-success")) :on-click #(swap! selected-players (fn [_] (update-selected-players p @selected-players)))} (p "loginname")]))

(defn select-players-view [course list-atom]
  (let [selected-players (r/atom #{})]
    (fn [] (if-let [players (not-empty @list-atom)]
      [panel "Select Players" #(select-course-page)
       [:div {:class list-item-class}
        [:button {:type "button" :class (str "btn btn-primary btn-sm btn-block " (cond (empty? @selected-players) "disabled" :else "")) :on-click #(start-game course @selected-players)} "Start"]
        (for [p players]
          [players-list-item p selected-players])]]
      [panel "Select Players" #(menu-page)
       [:div "Loading players..."]]))))

(defn course-page  [course]
  (let [c (r/atom course)]
    (get-resource (str courses-resource (course "id")) c)
    (render-page [course-view c])))

(defn player-page  [player]
  (let [c (r/atom player)]
    (get-resource (str players-resource (player "id")) c)
    (render-page [edit-player-view c])))

(defn games-list-page  []
  (get-resource games-resource games)
  (render-page [games-list-view games]))

(defn game-page [game]
  (.log js/console (str (keys game)))
  (let [c (r/atom nil)]
    (get-resource (str games-resource (game "id")) c (fn [resp] 
                                                       (let [scores (resp "scores") holes ((resp "course") "holes")]
                                                         (merge resp  {"scores-map" (into {} (map (fn [score] { #{(score "holes_id") (score "players_id")} (score "score")}) scores )) 
                                                                       "current-hole" (first holes)}))))
    (render-page [game-view c])))

(defn select-players-page  [course]
  (get-resource players-resource players)
  (render-page [select-players-view course players]))

(defn players-list-page  []
  (get-resource players-resource players)
  (render-page [players-list-view players]))

(defn courses-list-page []
  (get-resource courses-resource courses)
  (render-page [courses-list-view courses]))

(defn overview-page  [game-atom]
  (render-page
    [panel "Overview" #(menu-page)
     [overview-table-view @game-atom]]))

(defn statistics-page []
 (render-page
   (panel "Statistics" #(menu-page)
        [:div "stats"])))

(defn menu-page []
  (render-page [menu-view]))

(defn- by-id [id]
  (.getElementById js/document id))

(defn render-page [page]
  (r/render-component
    page
    (by-id "root")))

(defn render []
    (menu-page))


;; =============================================================================
;; Utilities
(defn events->chan
  "Given a target DOM element and event type return a channel of
  observed events. Can supply the channel to receive events as third
  optional argument."
  ([el event-type] (events->chan el event-type (chan)))
  ([el event-type c]
   (events/listen el event-type
     (fn [e] (put! c e)))
   c))

(defn mouse-loc->vec
  "Given a Google Closure normalized DOM mouse event return the
  mouse x and y position as a two element vector."
  [e]
  [(.-clientX e) (.-clientY e)])

(defn show!
  "Given a CSS id and a message string append a child paragraph element
  with the given message string."
  [id msg]
  (let [el (.getElementById js/document id)
        p  (.createElement js/document "p")]
    (set! (.-innerHTML p) msg)
    (.appendChild el p)))

(defn touch-chan []
      (events->chan (by-id "root") EventType.TOUCHEND))


(go
  (while true
    (let [msg (async/<! messages-channel)]
      (swap! messages conj msg)
      )))

(go
  (let [c (touch-chan)]
    (while true
      (async/<! c)
      (.log js/console "Touch Ended!"))))

(go
  (let [c (touch-chan)]
    (while true
      (async/<! c)
      (.log js/console "Touch Ended!"))))

