(ns scorecard-service.api
  (:use compojure.core
        liberator.core)
  (:require [compojure.route :as route]
            [compojure.handler :as handler]
            [ring.middleware.json :as middleware]
            [ring.middleware.params :as params]
            [liberator.dev :as dev]
            [net.cgrand.enlive-html :as enlive]
            [clojure.java.io :as io]
            [scorecard-service.middleware.cors :as cors]
            [scorecard-service.db :as db]
            [prone.middleware :as prone]))


(defn- request-body [context]
  (get-in context [:request :body]))


(defn- check-content-type [context content-types]
  (if (#{:put :post} (get-in context [:request :request-method]))
    (or
      (some #{(get-in context [:request :headers "content-type"])}
            content-types)
      [false {:message "Unsupported Content-Type"}])
    true))

(defresource player [id]
  :allowed-methods [:get :put :delete :options]
  :available-media-types ["application/json"]
  :can-put-to-missing? false
  :known-content-type? #(check-content-type % ["application/json" "application/json;charset=UTF-8"])
  :exists? (fn [context] (if-let [player (db/get-player id)] {::player player}))
  :put! (fn [context] (db/update-player id  (request-body context)))
  :delete! (fn [context] (db/delete-player id ))
  :handle-ok (fn [context] (clojure.core/dissoc (::player context) :created :modified :password)))

(defresource players []
  :method-allowed [:get]
  :available-media-types ["application/json"]
  :handle-ok (db/list-players))

(defresource course [id]
  :allowed-methods [:get :put :delete :options]
  :available-media-types ["application/json"]
  :can-put-to-missing? false
  :known-content-type? #(check-content-type % ["application/json" "application/json;charset=UTF-8"])
  :exists? (fn [context] (if-let [course (db/get-course id)] {::course course}))
  :put! (fn [context] (db/update-course id  (request-body context)))
  :delete! (fn [context] (db/delete-course id))
  :handle-ok ::course)

(defresource courses []
  :allowed-methods  [:get :post]
  :available-media-types ["application/json"]
  :handle-ok (db/list-courses))

(defresource game [id]
  :allowed-methods [:get :put :options]
  :available-media-types ["application/json"]
  :can-put-to-missing? false
  :known-content-type? #(check-content-type % ["application/json" "application/json;charset=UTF-8"])
  :exists? (fn [context] (if-let [game (db/get-game id)] {::game game}))
  :put! (fn [context] (db/update-game id  (request-body context)))
  :handle-ok (fn [context] (clojure.core/dissoc (::game context) :created :modified :id)))

(defresource games []
  :allowed-methods [:get :post :options]
  :available-media-types ["application/json"]
  :known-content-type? #(check-content-type % ["application/json" "application/json;charset=UTF-8"])
  :post! (fn [context] (if-let [id (db/new-game (request-body context))] {:id id}))
  :post-redirect?  (fn  [context]  {:location  (format  "/games/%s"  (:id context))})
  :handle-ok (db/list-games))


(enlive/deftemplate page
  "public/app.html"
  []
  [:body] (enlive/append
            (enlive/html [:script (browser-connected-repl-js)])))

(defroutes api-routes
  (context "/players" []
           (ANY "/" [] (players))
           (ANY "/:id" [id] (player id)))
  (context "/courses" []
           (ANY "/" [] (courses))
           (ANY "/:id" [id] (course id)))
  (context "/games" []
           (ANY "/" [] (games))
           (ANY "/:id" [id] (game id)))
  (route/resources "/resources"))

(defroutes site-routes
  (GET "/" req (page))
  (route/not-found "Resource not found!"))

(def api
  (->
      (handler/api api-routes)
      (middleware/wrap-json-body {:keywords? true})
      (params/wrap-params)
      (middleware/wrap-json-response)
      (prone/wrap-exceptions)))

(def site
  (handler/site site-routes))

(def handler (routes api site))
