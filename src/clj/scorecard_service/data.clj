(ns scorecard-service.data
  (:require 
    [scorecard-service.db-sql :as db-sql]
    [scorecard-service.channels :as c]
    [net.cgrand.enlive-html :as html]
    [clojure.data.json :as json]
    [clj-http.client :as client]
    [clojure.edn :as edn]
    [org.httpkit.client :as http-client]
    [clojure.tools.logging :as log]
    [clojure.core.async :as async :refer [>! go]]))

(def ^:dynamic *base-url* "http://frisbeegolfradat.fi/radat/")
(defonce data (atom {}))


(defn fetch-url [url]
  (let [current @data]
    (if (contains? current url)
      (current url)
      (let [content (html/html-resource (java.net.URL. url)) ]
        (swap! data #(assoc % url content))
        content))))

(defn extract-course-links []
  "Returns a lazy sequence containing course urls"
  []
  (let [table (html/select (fetch-url *base-url*) [:table#radatlistaus])
        rows (->> (html/select table [:tr])
                   (map #(html/select % [:td.rataCol :a]))
                   (map first)
                   (map #(select-keys % [:attrs]) )
                   (map #(get-in % [:attrs :href]) )
                   (filter seq))
        ]
    rows))

(defn first-content [item]
  (first (:content item)))

(defn parse-int [i]
   (try
     (Integer/parseInt i)
     (catch Exception e 0 )))

(defn parse-dist-and-par [s]
  (if-not (empty? s)
    (when-let  [ [_ dist _ _ par ] (clojure.string/split (first s) #"\s+")]
    {:distance (parse-int dist) :par (parse-int par)})))

(defn parse-hole [f]
  (let [[ number dist-and-par description] (:content f)]
    (merge
      {:number (parse-int (last (clojure.string/split (first (:content number)) #"\s+")))
       :description (str (first-content description))}
      (parse-dist-and-par  (:content dist-and-par)))))

(defn extract-course-html  [html]
  (let [ holes (map parse-hole (html/select html [:div.fairway]))
        name (first (:content (first (html/select html [:div.course-heading :h1])))) ]
    {:name name :description name :holes holes}))

(defn extract-course [url]
  (let [ html (html/html-resource (java.net.URL. url))]
      (extract-course-html html)))

(defn- spit-edn-test-data [filename data]
  (spit (str "data/" filename ".edn") (prn-str data)))

(defn- spit-course-data []
  (spit-edn-test-data "courses" (map extract-course (extract-course-links))))

(defn load-courses! []
  (doseq [course (take 4 (edn/read-string (slurp "data/courses.edn")))] 
    (go
      (>! c/persistence-chan course))))

(defn load-players! []
  (doseq [p (edn/read-string (slurp  "data/players.edn"))]
    (go
      (let [firstname (:firstname p) n-player (merge {:loginname firstname :password firstname} p)] 
        (>! c/persistence-chan-player n-player)))))

(defn reset-data! []
  (db-sql/reset-db!)
  (load-courses!)
  (load-players!))
