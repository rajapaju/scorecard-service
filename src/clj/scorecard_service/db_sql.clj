(ns scorecard-service.db-sql
  (:require
    [korma.db :as korma]
    [clojure.java.jdbc :as jdbc]))

(def db (korma/sqlite3 { :db "db/scorecard.db" }))

(defn create-db! []
  (try
    (jdbc/db-do-commands 
      db
      "PRAGMA foreign_keys = ON;"
      (jdbc/create-table-ddl :players
                             [:id "varchar(256)" "primary key"]
                             [:loginname :text "NOT NULL"]
                             [:password :text "NOT NULL"]
                             [:firstname :text]
                             [:lastname :text]
                             [:email :text]
                             [:created :timestamp "DEFAULT CURRENT_TIMESTAMP"]
                             [:modified :timestamp "DEFAULT CURRENT_TIMESTAMP"])
      (jdbc/create-table-ddl :courses
                             [:id "varchar(256)" "primary key"]
                             [:name :text "NOT NULL"]
                             [:description :text "NOT NULL"]
                             [:created :timestamp "DEFAULT CURRENT_TIMESTAMP"]
                             [:modified :timestamp "DEFAULT CURRENT_TIMESTAMP"])
      (jdbc/create-table-ddl :holes
                             [:id "varchar(256)" "primary key"]
                             [:courses_id "varchar(256)" "references courses(id)"]
                             [:description :text]
                             [:number :integer "NOT NULL"]
                             [:distance :integer]
                             [:par :integer "not null"]
                             [:created :timestamp "DEFAULT CURRENT_TIMESTAMP"]
                             [:modified :timestamp "DEFAULT CURRENT_TIMESTAMP"])
      (jdbc/create-table-ddl :games
                             [:id "varchar(256)" "primary key"]
                             [:courses_id "varchar(256)" "references courses(id)"]
                             [:created :timestamp "DEFAULT CURRENT_TIMESTAMP"]
                             [:modified :timestamp "DEFAULT CURRENT_TIMESTAMP"])
      (jdbc/create-table-ddl :gameplayers
                             [:id "varchar(256)" "primary key"]
                             [:games_id "varchar(256)" "references games(id)"]
                             [:players_id "varchar(256)" "references players(id)"])
      (jdbc/create-table-ddl :scores
                             [:games_id "varchar(256)" "references games(id)"]
                             [:players_id "varchar(256)" "references players(id)"]
                             [:holes_id "varchar(256)" "references holes(id)"]
                             [:score "integer not null"]))
    (catch Exception e (println e))))

(defn drop-db! []
  (doseq [table '(:courses :players :holes :games :gameplayers :scores)]
    (try
      (jdbc/db-do-commands db (jdbc/drop-table-ddl table))
      (catch Exception e (println e)))))

(defn reset-db! []
  (drop-db!)
  (create-db!))
