(ns scorecard_service.conf
  (:require [clojure.edn :as edn]))

(defonce conf (edn/read-string (slurp "conf/scorecard.edn")))
