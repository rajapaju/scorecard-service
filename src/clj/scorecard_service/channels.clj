(ns scorecard-service.channels
  (:use
   [clojure.core.async :only [chan]]))

(def persistence-chan (chan))
(def persistence-chan-player (chan))
(def course-chan (chan))
(def new-course (chan))
(def course-chan (chan))
(def courses-chan (chan))
