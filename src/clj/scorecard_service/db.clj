(ns scorecard-service.db
  (:use [korma.core]
        [korma.db])

  (:require
    [scorecard-service.db-sql :as db-sql]
    [scorecard-service.channels :as c]
    [clojure.java.jdbc :as jdbc]
    [korma.db :as kdb]
    [clojure.tools.logging :as log]
    [clojure.core.async :as async :refer [<! <!! >! >!! chan put! alts! timeout go go-loop]]))

(defdb korma-db db-sql/db)

(declare courses)
(declare games)
(defentity scores
  (belongs-to games))
(defentity players)
(defentity holes
  (belongs-to courses))
(defentity courses
  (has-many holes)
  (has-many games))
(defentity games
  (belongs-to courses)
  (many-to-many players :gameplayers)
  (has-many scores))
(defentity gameplayers
  (has-one games)
  (has-one players))

(defn- new-id []
  (str (java.util.UUID/randomUUID)))

(defn- merge-id [m]
  (merge {:id (new-id)} m))

(defn new-player  [player]
  (let [ n-player (merge-id player)]
    (log/info "Uusi pelaaja" n-player)
    (insert players (values n-player))))

(defn list-players []
  (select players (fields :loginname :id) (order :loginname) ))

(defn get-player [id]
  (log/info  "get-player" id)
  (first (select players (where {:id id}) )))

(defn update-player [id new-player]
  (update players (where {:id id}) (set-fields new-player)))

(defn delete-player [id]
  (delete players (where {:id id})))

;; Course

(defn list-courses []
  (log/info  "list courses")
  (select courses (fields :name :id) (order :name)))

(defn get-course [id]
  (log/info  "get course " id)
  (first (select courses
                  (with holes)
                  (where {:id id}))))

(defn update-course [id course]
  (update courses (where {:id id}) set-fields course))

(defn delete-course [id]
  (delete courses (where {:id id})))

; Game
(defn new-game [game]
  (let [id (new-id) 
        courseid (:courseid game)]
    (insert games (values {:id id :courses_id courseid}))
    (doseq [pid (game :playerids)]
      (insert gameplayers (values {:games_id id :players_id pid})))
    id))

(defn list-games []
  (log/info  "list  games")
  (select games (order :modified :DESC) (with courses)))

(defn get-game [id]
  (log/info  "Games " id)
  (let [game (first
               (select games  
                       (with players
                         (fields :id :loginname))
                       (with scores
                         (fields :holes_id :players_id :score))
                       (where {:id id})))]
   (merge game {"course" (get-course (game :courses_id))})))

(defn new-score [score game-id player-id hole-id]
  (insert scores (values {:games_id game-id 
                          :players_id player-id
                          :holes_id hole-id 
                          :score score})))

(defn insert-test-results "doc-string" []
  (new-score 7 "383e885c-6ec9-49c4-a115-d5834fa031a8" "9ec0d41a-8158-40c3-a944-65fc1e275ae6" "8f42c2af-c546-46a9-a2ed-080c91200bce"))

(defn update-game []
  (println "update-game"))
(go
  (while true
    (let [p (<! c/persistence-chan-player) ]
      (log/info "Työnnetään kantaan pelaaja: " p)
      (new-player p))))

(go
 (while true
   (let [course (<! c/persistence-chan)
         course-without-holes (dissoc course :holes)
         c-holes (:holes course)
         id (new-id)]
      (log/info "Käsitellään rataa " id)
      (insert courses (values (merge {:id id} course-without-holes)))
      (doseq [hole c-holes]
        (let [c-id (new-id) h (merge {:id c-id :courses_id id} hole)]
          (log/info "Käsitellään radan " id " väylää " c-id)
          (insert holes (values h)))))))


