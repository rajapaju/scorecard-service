(ns scorecard-service.fgrclient
  (:require [net.cgrand.enlive-html :as html]
            [clojure.data.json :as json]
            [org.httpkit.client :as http-client]
            [clojure.tools.logging :as log]
            [scorecard-service.channels :as c]
            [clojure.core.async :as async :refer [<! <!! >! >!! chan put! alts! timeout go go-loop]]))

(def ^:dynamic *base-url* "http://frisbeegolfradat.fi/radat/")

(defn async-get
  ([chan] (async-get *base-url* chan)) 
  ([url chan] 
   (log/info "Haetaan url " url)
   (http-client/get url #(go (>! chan (:body %))))))
