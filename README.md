# scorecard-service

   Clojure-toteutus scorecard-apista (linkki).

## Prerequisites

    https://github.com/technomancy/leiningen
    java

## Usage

    lein ring server

## Notes

    lein ancient :all
    Run tests ':Eval (clojure.test/run-tests)'
    http://clojure-doc.org/articles/tutorials/vim_fireplace.html

    lein kibit
    lein eastwood
    lein pikeshed

## License

    Copyright © 2014 Pajukoodi Oy
